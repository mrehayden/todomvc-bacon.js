APP = {};

var KEY_CODE_ENTER = 13;

APP.NewTodoInputModel = (function() {

  var me = {};

  // Public EventStream Bus for new todo input element to plug into.
  me.keydownStream = new Bacon.Bus();

  // Record when user submits by hitting return.
  me.submitStream = me.keydownStream
    .filter(function(event) {
      return event.which == KEY_CODE_ENTER;
    });

  me.valueStream = new Bacon.Bus();

  return me;

})();

APP.FilterModel = (function() {

  var me = {};

  me.filterStream = new Bacon.Bus();

  return me;

})();

APP.TodoModel = (function() {

  // When user submits non-empty string, add a new todo.
  var addTodoStream = APP.NewTodoInputModel.valueStream
    .toProperty()
    .sampledBy(APP.NewTodoInputModel.submitStream)
    .filter(function(value) {
      return value !== '';
    })
    .map(function(value) {
      return function(todos) {
        return todos.concat([{ completed: false, editing: false, value: value }]);
      };
    });

  var me = {};

  me.removeTodoStream = new Bacon.Bus();

  var removeTodoStream = me.removeTodoStream.map(function(i) {
    return function(todos) {
      todos.splice(i, 1);
      return todos;
    };
  });

  me.updateTodoStream = new Bacon.Bus();

  me.todoProperty = addTodoStream.merge(removeTodoStream)
    .merge(me.updateTodoStream)
    .scan([], function(todos, update) {
      return update(todos);
    });

  return me;

})();

APP.NewTodoInputView = (function() {

  var $el = $('#new-todo');

  // Plug input into new todo input model.
  APP.NewTodoInputModel.keydownStream.plug($el.asEventStream('keydown'));
  var valueStream = $el.asEventStream('keydown')
    .map(function() { return $el.val(); });
  APP.NewTodoInputModel.valueStream.plug(valueStream);

  // Clear the input when user submits.
  APP.NewTodoInputModel.submitStream.onValue(function() {
    $el.val('');
  });

})();

APP.TodoCountView = (function() {

  var $el = $('#todo-count');
  var template = Handlebars.compile($('#template-todo-count').html());

  APP.TodoModel.todoProperty.onValue(function(todos) {
    var count = todos.filter(function(todo) {
      return !todo.completed;
    }).length;
    var options = {
      plural: count == 1 ? false : true,
      count: count
    };
    $el.html(template(options));
  });

})();

APP.TodoListView = (function() {

  var $el = $('#todo-list');

  var todoProperty = APP.FilterModel.filterStream.flatMapLatest(function(activeFilter) {
    return APP.TodoModel.todoProperty.map(function(todos) {
      return todos.filter(function(todo) {
        if (activeFilter == 'completed') {
          return todo.completed;
        } else if (activeFilter == 'active') {
          return !todo.completed;
        }
        return true;
      });
    });
  });

  todoProperty.onValue(function(todos) {
    $el.empty();
    for (i in todos) {
      var todoItem = new APP.TodoListItemView(i, todos[i]);
      $el.append(todoItem.$el);
      if (todos[i].editing) {
        var $input = $('input.edit', todoItem.$el)
        $input.focus();
        // Reset the value to move the cursor to the end of the input.
        var value = $input.val();
        $input.val('');
        $input.val(value);
      }
    }
  });

})();

APP.TodoListItemView = function(index, todo) {

  var template = Handlebars.compile($('#template-todo-list-item').html());

  var classes = [];
  if (todo.completed) {
    classes.push('completed');
  }
  if (todo.editing) {
    classes.push('editing');
  }

  this.$el = $(template({
    classes: classes,
    completed: todo.completed,
    value: todo.value
  }));

  // Bind listeners
  var destroyButtonClickStream = $('.destroy', this.$el)
    .asEventStream('click')
    .map(function(event) {
      return index;
    });
  APP.TodoModel.removeTodoStream.plug(destroyButtonClickStream);

  var completedClickStream = $('.toggle', this.$el)
    .asEventStream('click')
    .map(function() {
      return function(todos) {
        todos[index].completed = !todos[index].completed;
        return todos;
      };
    });
  APP.TodoModel.updateTodoStream.plug(completedClickStream);

  var editFocusClickStream = $('label', this.$el)
    .asEventStream('dblclick')
    .map(function() {
      // Enable an active todo's input when the label is clicked.
      return function(todos) {
        if (!todos[index].completed) {
          todos[index].editing = true;
        }
        return todos;
      };
    });
  APP.TodoModel.updateTodoStream.plug(editFocusClickStream);

  var editBlurClickStream = $('input.edit', this.$el)
    .asEventStream('blur')
    .map(function(event) {
      return function(todos) {
        todos[index].editing = false;
        todos[index].value = $(event.target).val();
        return todos;
      };
    });
  APP.TodoModel.updateTodoStream.plug(editBlurClickStream);

};

APP.FilterButtonView = function(id, active) {

  var template = Handlebars.compile($('#template-filter-button').html());
  var labels = { all: 'All', active: 'Active', completed: 'Completed' };
  var classes = active ? ['selected'] : [];

  this.$el = $(template({ id: id, classes: classes, label: labels[id] }));

  // Attach listeners.
  var clickStream = this.$el.asEventStream('click')
    .map(function() {
      return id;
    });
  APP.FilterModel.filterStream.plug(clickStream);

};

APP.FilterButtonsView = (function() {

  var $el = $('#filters');
  var filters = ['all', 'active', 'completed'];

  APP.FilterModel.filterStream.onValue(function(activeFilter) {
    $el.empty();
    for (i in filters) {
      var id = filters[i];
      var button = new APP.FilterButtonView(id, activeFilter == id);
      $el.append(button.$el);
    }
  });

  APP.FilterModel.filterStream.push('all');

})();

APP.ClearCompletedButtonView = (function() {

  var $el = $('#clear-completed');

  var clickEventStream = $el.asEventStream('click')
    .map(function() {
      return function(todos) {
        return todos.filter(function(todo) {
          return !todo.completed;
        });
      };
    });

  APP.TodoModel.updateTodoStream.plug(clickEventStream);

})();
